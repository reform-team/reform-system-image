// performance tweaks
pref("layers.acceleration.force-enabled", true);
pref("general.smoothScroll", false);

// remove ads
pref("browser.newtabpage.activity-stream.feeds.topsites", false);
pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
pref("browser.topsites.contile.enabled", false);
