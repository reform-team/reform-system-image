#!/bin/bash
# Copyright 2018-2023 Lukas F. Hartmann / MNT Research GmbH
# Copyright 2021-2023 Johannes Schauer Marin Rodrigues <josch@mister-muffin.de>
# SPDX-License-Identifier: GPL-3.0-only

# shellcheck disable=SC2016 # Intentional quoting technique

set -eu

export LC_ALL=C.UTF-8

: "${DIST:=unstable}"
: "${MIRROR:=mntre.com}"
: "${QUICK:=no}"

nth_arg() {
	shift "$1"
	printf "%s" "$1"
}

usage() {
	echo "usage: $0 [--quick] [-m mirror] [-d dist] [board,...]" >&2
	exit 1
}

usage_error() {
	echo "error: $*" 1>&2
	usage
}

while getopts :h:d:m:-: OPTCHAR; do
	case "$OPTCHAR" in
		d) DIST="$OPTARG"   ;;
		m) MIRROR="$OPTARG" ;;
		h) usage            ;;
		-)	case "$OPTARG" in
				help)     usage                                                    ;;
				quick)    QUICK=yes                                                ;;
				dist)     DIST="$(nth_arg "$OPTIND" "$@")";   OPTIND=$((OPTIND+1)) ;;
				dist=*)   DIST="${OPTARG#*=}"                                      ;;
				mirror)   MIRROR="$(nth_arg "$OPTIND" "$@")"; OPTIND=$((OPTIND+1)) ;;
				mirror=*) MIRROR="${OPTARG#*=}"                                    ;;
				*)        echo "unrecognized option --$OPTARG">&2; exit 1          ;;
			esac ;;
		:)   usage_error "missing argument for -$OPTARG" ;;
		'?') usage_error "unrecognized option -$OPTARG"  ;;
		*)   echo "internal error while parsing command options">&2; exit 1;;
	esac
done
shift "$((OPTIND - 1))"

if [ "$#" -eq 0 ]; then
	if [ -z "${SYSIMAGES:-}" ]; then
		# SYSIMAGES is unset or empty
		SYSIMAGES="reform-system-imx8mq reform-system-ls1028a"
		case "$DIST" in
			bookworm)           : ;;
			bookworm-backports) SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp reform-system-rk3588 reform-next-system-rk3588 pocket-reform-system-rk3588";;
			testing)            SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp reform-system-rk3588 reform-next-system-rk3588 pocket-reform-system-rk3588";;
			unstable)           SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp reform-system-rk3588 reform-next-system-rk3588 pocket-reform-system-rk3588";;
			experimental)       SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp reform-system-rk3588 reform-next-system-rk3588 pocket-reform-system-rk3588";;
			*) echo "unsupported distribution: $DIST" >&2; exit 1;;
		esac
	else
		for SYSIMAGE in $SYSIMAGES; do
			case "$SYSIMAGE" in
				pocket-reform-system-a311d) : ;;
				pocket-reform-system-imx8mp) : ;;
				reform-system-a311d) : ;;
				reform-system-imx8mp) : ;;
				reform-system-imx8mq) : ;;
				reform-system-ls1028a) : ;;
				reform-system-rk3588) : ;;
				reform-next-system-rk3588) : ;;
				pocket-reform-system-rk3588) : ;;
				*)
					echo "unsupported image: $SYSIMAGE" >&2
					echo >&2
					echo "List of supported images:" >&2
					echo >&2
					echo "pocket-reform-system-a311d" >&2
					echo "pocket-reform-system-imx8mp" >&2
					echo "reform-system-a311d" >&2
					echo "reform-system-imx8mp" >&2
					echo "reform-system-imx8mq" >&2
					echo "reform-system-ls1028a" >&2
					echo "reform-system-rk3588" >&2
					echo "reform-next-system-rk3588" >&2
					echo "pocket-reform-system-rk3588" >&2
					exit 1
					;;
			esac
		done
	fi
else
	SYSIMAGES=
	while [ "$#" -gt 0 ]; do
		case "$1" in
			pocket-reform-system-a311d) : ;;
			pocket-reform-system-imx8mp) : ;;
			pocket-reform-system-rk3588) : ;;
			reform-next-system-rk3588) : ;;
			reform-system-a311d) : ;;
			reform-system-imx8mp) : ;;
			reform-system-imx8mq) : ;;
			reform-system-ls1028a) : ;;
			reform-system-rk3588) : ;;
			*)
				echo "unsupported image: $1" >&2
				echo >&2
				echo "List of supported images:" >&2
				echo >&2
				echo "pocket-reform-system-a311d" >&2
				echo "pocket-reform-system-imx8mp" >&2
				echo "reform-system-a311d" >&2
				echo "reform-system-imx8mp" >&2
				echo "reform-system-imx8mq" >&2
				echo "reform-system-ls1028a" >&2
				echo "reform-system-rk3588" >&2
				echo "reform-next-system-rk3588" >&2
				echo "pocket-reform-system-rk3588" >&2
				exit 1
				;;
		esac
		if [ "$DIST" = bookworm ]; then
			case "$1" in reform-system-imx8mq|reform-system-ls1028a) : ;;
				*) echo "unsupported image on bookworm: $1" >&2; exit 1 ;;
			esac
		fi
		SYSIMAGES="$SYSIMAGES $1"
		shift
	done
fi

case "$MIRROR" in
	reform.debian.net)
		case "$DIST" in bookworm|bookworm-backports) : ;; *)
			echo "reform.d.n only supports bookworm and bookworm-backports" >&2
			exit 1
		esac
		;;
	mntre.com)
		case "$DIST" in testing|unstable|experimental) : ;; *)
			echo "mntre.com only supports testing, unstable and experimental" >&2
			exit 1
		esac
		;;
	*) echo "unsupported mirror: $MIRROR" >&2; exit 1;;
esac

# make sure build tools are installed
depends="mmdebstrap (>= 0.8.4-1), e2fsprogs (>= 1.47.1~rc2-1~), git, mount, parted, python3-apt, bmap-tools, libarchive13"
if [ "$(dpkg --print-architecture)" != arm64 ]; then
	depends="$depends, arch-test, qemu-user-static"
fi
case "$MIRROR" in
reform.debian.net) depends="$depends, xz-utils" ;;
mntre.com)         depends="$depends, wget, pigz" ;;
esac
# We use dpkg-checkbuilddeps to check whether the script dependencies are
# installed or otherwise we will not be able to check whether potentially
# virtual packages like libarchive13 (provided by libarchive13t64 on 64-bit
# architectures) are installed or not.
# It also makes it easier to check version constrains than calling
# "dpkg --compare-versions $(dpkg-query --showformat='${Version}\n' --show ..."
if [ "$(dpkg-query -f '${db:Status-Status}' -W "dpkg-dev")" != installed ]; then
	echo "please install dpkg-dev"
	exit 1
fi
if ! dpkg-checkbuilddeps -d "$depends" /dev/null; then
	echo "Not all dependencies of this script are satisfied."
	echo "Run the following command to install them:"
	echo
	echo "    sudo apt satisfy \"$depends\""
	exit 1
fi

# sanity checks passed, be more verbose from now on
set -x

# if we are in a git repository and if SOURCE_DATE_EPOCH is not set, use the
# timestamp of the latest git commit
if [ -z ${SOURCE_DATE_EPOCH+x} ]; then
	if git -C . rev-parse 2>/dev/null; then
		SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)
	else
		SOURCE_DATE_EPOCH=$(date +%s)
	fi
fi
export SOURCE_DATE_EPOCH

# git only stores executable permissions, the rest is up to the umask of the
# process that cloned the git repo. To make sure that the files in /etc are
# not world-writable, we fix up the permissions manually
for f in . firefox firefox/syspref.js motd-full motd-rescue profile.d profile.d/reform.sh; do
  chmod "go-w" "./etc/$f"
done
chmod -R "go-w" "./etc/skel"
chmod "go-w" reform-setup-cleanup.sh reform-setup-sway-config reform-setup.sh
# Make sure that the default permission bits for files we create from here on
# out do not allow others to write to these files.
umask 022

# We need a separate partition for /boot for two reasons:
#
# 1) To boot encrypted NVMe
# 2) If we boot a system that is not on the SD-Card (even unencrypted) then we
#    need to mount the partition containing kernel, initrd and dtb. If /boot is
#    not in its own partition, then mounting that partition to /boot will
#    result in the files being in /boot/boot. So we need to create a partition
#    where kernel, initrd and dtb are at the root of the partition. Otherwise
#    we cannot upgrade the kernel from the booted system.

# debian-installer chooses 999424 sectors (= 488MB) by default
BOOTSIZE=488
ROOTSIZE=4096

rm -f ./machines/*.conf
mkdir -p ./machines ./repo

# create Packages and Release file for custom repo
if [ -n "$(find repo -maxdepth 1 -name "*.deb" -print -quit)" ]; then
	echo "I: Creating repository from *.deb in ./repo and serving via HTTP" >&2
	env --chdir=./repo apt-ftparchive packages . > ./repo/Packages
	env --chdir=./repo apt-ftparchive -o APT::FTPArchive::Release::Codename=reform-local-repo -o APT::FTPArchive::Release::Label=reform-local-repo release . > ./repo/Release
	# use setpriv to automatically send TERM when this script exits
	setpriv --pdeathsig TERM python3 -m http.server -d repo &
else
	echo "I: No *.deb files found in ./repo" >&2
fi

# write out initramfs premount script
cat << 'END' > reform-firstboot-premount
#!/bin/sh
set -e
case $1 in prereqs) echo; exit 0;; esac
. /scripts/functions
if [ "$ROOT" != LABEL=reformsdroot ]; then log_warning_msg "$0: unexpected ROOT: $ROOT"; exit 0; fi
rootpart=$(realpath /dev/disk/by-label/reformsdroot)
case $rootpart in
	/dev/vda*) : ;; # qemu
	/dev/mmcblk*) : ;; # sd-card or emmc
	*) log_warning_msg "$0: unexpected disk: $rootpart"; exit 0 ;;
esac
rootpart_nr=$(blkid --match-tag PART_ENTRY_NUMBER --output value --probe "$rootpart")
rootdev="/dev/$(lsblk --noheadings --nodeps --output PKNAME "$rootpart")"
# read four bytes, starting with byte number 441 (position 440)
if [ "$(tail -c +441 "$rootdev" | head -c 4)" != "mntr" ]; then log_warning_msg "$0: unexpected disk sig"; exit 0; fi
if [ "$(blkid --match-tag TYPE --output value --probe "$rootpart")" != "ext4" ]; then log_warning_msg "$0: unexpected fs type"; exit 0; fi
if [ "$(blkid --match-tag LABEL --output value --probe "$rootpart")" != "reformsdroot" ]; then log_warning_msg "$0: unexpected filesystem label"; exit 0; fi
if test -z "$(parted -m -s "$rootdev" print free | tail -n1 | grep free)"; then log_warning_msg "$0: no free space to fill"; exit 0; fi
log_begin_msg "$0 resizing $ROOT"
# Expand the partition size to fill the entire device
parted -s "$rootdev" resizepart "$rootpart_nr" 100%
wait_for_udev 5
partprobe "$rootdev"
# fill partition
resize2fs -p "$rootpart"
log_end_msg
END
chmod +x reform-firstboot-premount
cat << 'END' > reform-firstboot-hook
#!/bin/sh
set -e
case $1 in prereqs) echo; exit 0;; esac
. /usr/share/initramfs-tools/hook-functions

copy_exec /sbin/blkid
copy_exec /bin/lsblk
copy_exec /sbin/parted
copy_exec /sbin/partprobe
copy_exec /sbin/resize2fs
copy_exec /sbin/fsck
copy_exec /sbin/logsave
copy_exec /sbin/fsck.ext2
copy_exec /sbin/fsck.ext4
copy_exec /sbin/e2fsck
copy_exec /sbin/tune2fs
END
chmod +x reform-firstboot-hook
cat << 'END' > reform-firstboot.service
[Unit]
Description=Remove firstboot initramfs hooks and scripts

[Service]
Type=oneshot
ExecStart=/usr/bin/rm -f /etc/systemd/system/%n /etc/systemd/system/multi-user.target.wants/%n

[Install]
WantedBy=multi-user.target
END
cat << 'END' > reform-mkimage-hook
#!/bin/sh
set -e
case $1 in prereqs) echo; exit 0;; esac
. /usr/share/initramfs-tools/hook-functions

# LS1028A
copy_file firmware /boot/ls1028a-mhdpfw.bin /lib/firmware
copy_exec /usr/bin/dmesg

# Pocket Reform
copy_exec /usr/bin/setfont
copy_file Uni2-TerminusBold32x16 /usr/share/consolefonts/Uni2-TerminusBold32x16.psf.gz
END
chmod +x reform-mkimage-hook

tar --owner=root:0 --group=root:0 --directory etc --create --file etc.tar .

# build the debian userland and configure it

# fill $@ array with options passed to mmdebstrap
set -- --architectures=arm64 --components=main,non-free-firmware --variant="minbase" --verbose
if [ "$QUICK" != "yes" ]; then
	# packages for graphical user interface
	set -- "$@" --include="xwayland xterm sway fonts-inter fonts-noto-color-emoji waybar swayidle swaylock mesa-utils lxpolkit wayland-protocols wofi wireplumber papirus-icon-theme libglib2.0-bin gsettings-desktop-schemas gnome-disk-utility gnome-themes-extra-data gnome-icon-theme gnome-settings-daemon gnome-system-monitor grim slurp gedit evince mpv sxiv gvfs-backends unicode-data engrampa neverball minetest qt5ct kde-style-breeze dunst pasystray wev wf-recorder wayvnc network-manager-gnome blueman pavucontrol thunar"
	# reform-tools recommends
	set -- "$@" --include=" bmap-tools brightnessctl fonts-font-awesome fonts-jetbrains-mono foot gir1.2-ayatanaappindicator3-0.1 gir1.2-gdesktopenums-3.0 gir1.2-gdkpixbuf-2.0 gir1.2-glib-2.0 gir1.2-gtk-3.0 gir1.2-gtklayershell-0.1 gir1.2-notify-0.7 gnome-system-monitor ircii pavucontrol pulseaudio-utils python3 python3-gi python3-usb1 thunar x11-xserver-utils"
	case "$DIST" in
		bookworm) set -- "$@" --include="firefox-esr" ;;
		bookworm-backports) set -- "$@" --include="firefox-esr" ;;
		*) set -- "$@" --include="firefox" ;;
	esac
fi
case "$DIST" in
	bookworm) : ;;
	bookworm-backports) set -- "$@" --include="tuigreet" ;;
	*) set -- "$@" --include="wayfire reform-firedecor tuigreet" ;;
esac
# packages for networking
set -- "$@" --include="iproute2 iptables inetutils-ping elinks isc-dhcp-client netcat-traditional net-tools network-manager nfacct ntp ntpdate rsync telnet traceroute wpasupplicant curl wget w3m rfkill ifupdown netbase openssh-client firmware-realtek wireless-regdb wavemon iw"
case "$DIST" in
	bookworm) : ;; # no reform-qcacld2 in bookworm
	bookworm-backports)
		# For rk3588, /lib/firmware/arm/mali/arch10.8/mali_csffw.bin is needed
		# which requires firmware-misc-nonfree from bookworm-backports
		set -- "$@" --include=firmware-misc-nonfree/bookworm-backports
		# For rk3588, without mesa from backports, llvmpipe is used for software
		# rendering which is slow and buggy. Use panfrost from backports instead.
		set -- "$@" --include=libgl1-mesa-dri/bookworm-backports,libgbm1/bookworm-backports,libdrm-amdgpu1/bookworm-backports,libdrm-radeon1/bookworm-backports,libglapi-mesa/bookworm-backports,libegl-mesa0/bookworm-backports,libglx-mesa0/bookworm-backports
		# still in NEW --include=ezurio-qca-firmware
		set -- "$@" --include=ezurio-qcacld-2.0-dkms
		;;
	*)
		# firmware-mediatek got split out of firmware-misc-nonfree in trixie
		set -- "$@" --include="reform-qcacld2 firmware-mediatek" ;;
esac
# packages for system administration
set -- "$@" --include="apt apt-utils apt-listbugs apt-file cron cryptsetup lvm2 dbus-bin e2fsprogs fbset init-system-helpers ncdu parted pciutils procps sudo systemd systemd-sysv tmux u-boot-tools screen greetd"
# utilities
set -- "$@" --include="busybox console-data console-setup cpio file flash-kernel gnupg gpgv htop kbd lm-sensors readline-common usbutils xdg-utils bsdmainutils less nano micro vim alsa-utils dosfstools python3-psutil unzip nvme-cli fdisk lshw hdparm"
case "$DIST" in
	bookworm) : ;;             # reform-handbook needs libjs-sphinxdoc (>= 7.4)
	bookworm-backports) : ;;   # and picotool is new in trixie
	*) set -- "$@" --include="reform-handbook pocket-reform-handbook picotool" ;;
esac
# miscellaneous
set -- "$@" --include="brightnessctl brightness-udev ca-certificates debian-archive-keyring dialog gpm ncurses-term locales bash-completion man-db cryptsetup-initramfs reform-tools gpiod firmware-misc-nonfree firmware-atheros zstd"
case "$DIST" in
	bookworm) set -- "$@" --include="linux-image-arm64 linux-headers-arm64" ;;
	bookworm-backports) set -- "$@" --include="linux-image-mnt-reform-arm64 linux-headers-mnt-reform-arm64 gpiod/bookworm-backports" ;;
	*) set -- "$@" --include="linux-image-mnt-reform-arm64 linux-headers-mnt-reform-arm64" ;;
esac
# apt preferences
set -- "$@" --setup-hook='mkdir -p "$1"/etc/apt/sources.list.d'
case "$MIRROR" in
	mntre.com)
		set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: release n=reform, l=reform"; echo "Pin-Priority: 990"; } > "$1"/etc/apt/preferences.d/reform.pref' \
			--setup-hook='copy-in mntre.sources /etc/apt/sources.list.d/' \
			--setup-hook='chown 0:0 "$1"/etc/apt/sources.list.d/mntre.sources' \
			--setup-hook='chmod 644 "$1"/etc/apt/sources.list.d/mntre.sources'
		;;
	reform.debian.net)
		set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: origin \"reform.debian.net\""; echo "Pin-Priority: 999"; } > "$1"/etc/apt/preferences.d/reform.pref' \
			--setup-hook='copy-in reform_bookworm.sources /etc/apt/sources.list.d/' \
			--setup-hook='chown 0:0 "$1"/etc/apt/sources.list.d/reform_bookworm.sources' \
			--setup-hook='chmod 644 "$1"/etc/apt/sources.list.d/reform_bookworm.sources'
		;;
esac
case "$DIST" in
	bookworm-backports)
		set -- "$@" --setup-hook='copy-in reform_bookworm-backports.sources /etc/apt/sources.list.d/' \
			--setup-hook='chown 0:0 "$1"/etc/apt/sources.list.d/reform_bookworm-backports.sources' \
			--setup-hook='chmod 644 "$1"/etc/apt/sources.list.d/reform_bookworm-backports.sources' \
			--setup-hook='echo deb http://deb.debian.org/debian bookworm-backports main non-free-firmware > "$1"/etc/apt/sources.list.d/bookworm-backports.list'
		;;
	experimental) set -- "$@" --setup-hook='echo deb http://deb.debian.org/debian experimental main non-free-firmware > "$1"/etc/apt/sources.list.d/experimental.list';;
esac
case "$DIST" in
	bookworm|bookworm-backports)
		# reform-branding is backported into the reform.d.n repo
		set -- "$@" --include=reform-branding
		;;
	*)
		# install reform-branding from non-free (the package is licensed under CC-BY-NC)
		# download manually to avoid adding non-free to the apt sources
		set -- "$@" --customize-hook='/usr/lib/apt/apt-helper download-file \
			"https://deb.debian.org/debian/pool/non-free/r/reform-branding/reform-branding_4_all.deb" \
			"$1/reform-branding_4_all.deb" "SHA256:385e756c56fe28d213e57367720f293e1f0e2a24e1f508b50068a3bf5b18f2fd"' \
			--customize-hook='chroot "$1" apt-get install --yes ./reform-branding_4_all.deb' \
			--customize-hook='rm "$1/reform-branding_4_all.deb"'
		;;
esac
# sanity check that we only have a single kernel image installed
set -- "$@" --customize-hook='test "$(find "$1/lib/modules" -maxdepth 1 -mindepth 1 -type d | wc -l)" -eq 1'
# copy out machine descriptions from reform-tools so that we can use them as well
set -- "$@" --customize-hook="sync-out /usr/share/reform-tools/machines ./machines"
# list packages and versions that are not from debian.org
set -- "$@" --customize-hook="APT_CONFIG=\$MMDEBSTRAP_APT_CONFIG python3 -c 'import apt_pkg;apt_pkg.init();cache=apt_pkg.Cache(None);print(\"\n\".join(sorted([f\"{pkg.name:<44} {pkg.current_ver.ver_str}\" for pkg in cache.packages if pkg.current_ver for pfile, _ in pkg.current_ver.file_list if not pfile.not_source and not pfile.site.endswith(\".debian.org\")])))' > \"\$1\"/custom-pkgs.lst" \
		--customize-hook='copy-out /custom-pkgs.lst .' \
		--customize-hook='rm "$1"/custom-pkgs.lst'
# set up local repo if necessary
if [ -n "$(find repo -maxdepth 1 -name "*.deb" -print -quit)" ]; then
	# use Pin-Priority: 1000 to install from this repo even if it would
	# cause a downgrade
	set -- "$@" \
		--setup-hook='{ echo "Package: *"; echo "Pin: release n=reform-local-repo, l=reform-local-repo"; echo "Pin-Priority: 1000"; } > "$1"/etc/apt/preferences.d/reform-local-repo.pref' \
		--customize-hook='rm "$1"/etc/apt/preferences.d/reform-local-repo.pref' \
		--setup-hook='echo "deb [arch=arm64 trusted=yes] http://127.0.0.1:8000/ ./" > "$1"/etc/apt/sources.list.d/reform-local-repo.list' \
		--customize-hook='rm "$1"/etc/apt/sources.list.d/reform-local-repo.list'
fi
# temporarily disable dkms signing (it's unreproducible)
set -- "$@" --setup-hook='mkdir -p "$1/etc/dkms/framework.conf.d/"' \
		--setup-hook='echo "sign_file=\"does-not-exist\"" > "$1/etc/dkms/framework.conf.d/nosigning.conf"'
# The board specific kernel commandline arguments now live in
# /usr/share/flash-kernel/ubootenv.d/00reform2_ubootenv which does the
# platform detection and then adds the options to ${bootargs}.
# We still want to overwrite /etc/default/flash-kernel because by default it will contain
# LINUX_KERNEL_CMDLINE="quiet" and we don't want that
set -- "$@" --essential-hook='{ echo LINUX_KERNEL_CMDLINE=\"\"; echo LINUX_KERNEL_CMDLINE_DEFAULTS=\"\"; } > "$1"/etc/default/flash-kernel'
# select timezone and locales
# this is later customized by the Reform Setup Wizard GUI
set -- "$@" \
	--essential-hook='echo tzdata tzdata/Areas select Europe | chroot "$1" debconf-set-selections' \
	--essential-hook='echo tzdata tzdata/Zones/Europe select Berlin | chroot "$1" debconf-set-selections' \
	--essential-hook='echo locales locales/default_environment_locale select en_US.UTF-8 | chroot "$1" debconf-set-selections' \
	--essential-hook='echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | chroot "$1" debconf-set-selections' \
	--essential-hook='echo console-setup console-setup/fontface47 select Do not change the boot/kernel font | chroot "$1" debconf-set-selections' \
	--essential-hook='echo console-setup console-setup/fontsize string | chroot "$1" debconf-set-selections'

# set up motd
set -- "$@" \
	--customize-hook='rm -f "$1"/etc/motd' \
	--customize-hook='ln -s motd-full "$1"/etc/motd'
if [ "$QUICK" != "yes" ]; then
	# configure default terminal
	set -- "$@" \
		--customize-hook='rm "$1/etc/alternatives/x-terminal-emulator"' \
		--customize-hook='ln -s /usr/bin/foot "$1/etc/alternatives/x-terminal-emulator"'
fi
# populate root user and skel
set -- "$@" \
	--customize-hook='tar-in etc.tar /etc/' \
	--customize-hook='for d in Desktop Documents Downloads Music Pictures Videos; do mkdir -p "$1/etc/skel/$d"; done' \
	--customize-hook='for m in full rescue; do sed -i "s/{DATE}/$(date --date="@$SOURCE_DATE_EPOCH" --iso-8601=date)/" "$1/etc/motd-$m"; chown 0:0 "$1/etc/motd-$m"; done' \
	--customize-hook='echo '"'"'if [ "$(whoami)" = "root" ]; then reform-help --root; elif [ -z "$WAYLAND_DISPLAY" ]; then reform-help; fi'"'"' >> "$1"/etc/skel/.profile' \
	--customize-hook='chroot "$1" sh -c "rsync -Pha /etc/skel/ /root"'
# start setup wizard by default (this script will remove itself)
case "$MIRROR" in
	mntre.com)
		wget -O reform-setup "https://source.mnt.re/reform/mnt-reform-setup-wizard/-/jobs/artifacts/main/raw/target/aarch64-unknown-linux-gnu/release/reform-setup?job=build-cross"
		chmod 755 reform-setup
		set -- "$@" \
			--chrooted-customize-hook='systemctl --root=/ disable greetd' \
			--customize-hook='mkdir -p "$1"/etc/systemd/system/getty@tty1.service.d' \
			--customize-hook='printf "[Service]\nExecStart=\nExecStart=-/sbin/agetty --autologin root --noclear %%I \$TERM\n" > "$1"/etc/systemd/system/getty@tty1.service.d/override.conf' \
			--customize-hook='mkdir -p "$1"/usr/share/reform-setup-wizard/cleanup.d' \
			--customize-hook='copy-in reform-setup /usr/bin' \
			--customize-hook='copy-in reform-setup.sh /etc/profile.d' \
			--customize-hook='copy-in reform-setup-sway-config /usr/share/reform-setup-wizard' \
			--customize-hook='copy-in reform-setup-cleanup.sh /usr/share/reform-setup-wizard/cleanup.d' \
			--customize-hook='chmod 755 "$1"/usr/bin/reform-setup' \
			--customize-hook='chmod 755 "$1"/usr/share/reform-setup-wizard/cleanup.d/*' \
			--customize-hook='chown 0:0 "$1"/usr/bin/reform-setup "$1"/etc/profile.d/reform-setup.sh "$1"/usr/share/reform-setup-wizard/reform-setup-sway-config "$1"/usr/share/reform-setup-wizard/cleanup.d/reform-setup-cleanup.sh'
		;;
	reform.debian.net)
		case "$DIST" in
			bookworm) : ;; # do nothing for bookworm
			bookworm-backports) : ;; # FIXME: needs reform-setup-wizard in backports
		esac
		;;
esac
# populate /etc
set -- "$@" \
	--essential-hook='mkdir -p "$1"/etc/initramfs-tools/hooks "$1"/etc/initramfs-tools/scripts/local-premount' \
	--essential-hook='copy-in reform-firstboot-premount /etc/initramfs-tools/scripts/local-premount/' \
	--essential-hook='copy-in reform-firstboot-hook /etc/initramfs-tools/hooks/' \
	--customize-hook='copy-in reform-firstboot.service /etc/systemd/system/' \
	--customize-hook='chown 0:0 "$1"/etc/initramfs-tools/scripts/local-premount/reform-firstboot-premount "$1"/etc/initramfs-tools/hooks/reform-firstboot-hook "$1"/etc/systemd/system/reform-firstboot.service' \
	--chrooted-customize-hook='systemctl --root=/ enable reform-firstboot.service' \
	--essential-hook='{ echo LABEL=reformsdroot / auto errors=remount-ro 0 1; echo LABEL=reformsdboot /boot auto errors=remount-ro 0 1; } > "$1"/etc/fstab' \
	--customize-hook='echo reform > "$1"/etc/hostname' \
	--customize-hook='{ echo 127.0.0.1 localhost reform; echo ::1 localhost ip6-localhost ip6-loopback reform; echo ff02::1 ip6-allnodes; echo ff02::2 ip6-allrouters; } > "$1"/etc/hosts'
# or else EXTRA_GROUPS doesnt work?
set -- "$@" \
	--customize-hook='sed -i "s/^#EXTRA_GROUPS=.*/EXTRA_GROUPS=audio cdrom dip floppy video plugdev netdev/" "$1"/etc/adduser.conf' \
	--customize-hook='sed -i "s/^#ADD_EXTRA_GROUPS=.*/ADD_EXTRA_GROUPS=1/" "$1"/etc/adduser.conf'
# Make sure that /usr/share/kernel/postinst.d/zz-reform-tool copied everything
# The flash-kernel package is still needed to provide the /boot/dtb-$(uname -r)
# symlink. This symlink is needed on platforms that do not set the `${fdtfile}`
# u-boot environment variable.
set -- "$@" --chrooted-customize-hook="
set -e;
rm -f /boot/boot.scr /boot/boot.scr.bak;
FK_DIR=\"/usr/share/flash-kernel\";
. /usr/share/flash-kernel/functions;
set -x;
mkarch=\"arm64\";
tmpdir=\"\$(mktemp -d)\";
kvers=\"\$(linux-version list | linux-version sort | tail -1)\";
mkimage_script \"\" \"boot script\" /etc/flash-kernel/bootscript/bootscr.uboot-generic /boot/boot.scr;
set -u;
# if the outer systems happens to be supported by flash-kernel, then these
# symlinks were written and we remove them to avoid contaminating the chroot
rm -f /boot/dtb \"/boot/dtb-\$kvers\"
bookworm_support() {
    case \$1 in
        reform-system-imx8mq) return 0;;
        reform-system-ls1028a) return 0;;
        *) return 1;;
    esac;
};
find /boot/dtbs | sort >&2;
for CONF in /usr/share/reform-tools/machines/*.conf; do
    . \"\$CONF\";
    if [ \"$DIST\" = \"bookworm\" ] && ! bookworm_support \"\$SYSIMAGE\"; then
           continue;
    fi;
    echo \"\$DTBPATH\" >&2;
    dtb=\$(find \"/usr/lib/linux-image-\$kvers\" -wholename \"*/\$DTBPATH\" 2>/dev/null | head -n 1);
    echo \"\$dtb\" >&2;
    test -f \"\$dtb\";
    dtb_dir=\"\$(dirname \"\$DTBPATH\")\";
    cmp \"\$dtb\" \"/boot/dtbs/\$kvers/\$DTBPATH\";
done;
rm -r \"\$tmpdir\";
"
# download DisplayPort firmware blob for LS1028A
set -- "$@" --essential-hook='/usr/lib/apt/apt-helper download-file \
	"https://source.mnt.re/reform/reform-ls1028a-uboot/-/raw/main/ls1028a-mhdpfw.bin" \
	"$1/boot/ls1028a-mhdpfw.bin" "SHA1:fa96b9aa59d7c1e9e6ee1c0375d0bcc8f8e5b78c"'
# Copy in an initramfs hook which does device specific setup.
# This is to make sure that the initramfs we create here also contains
# ls1028a-mhdpfw.bin and /usr/bin/dmesg for LS1028A or /usr/bin/setfont and
# Uni2-TerminusBold32x16 for the Pocket Reform. Alternatively, we could also
# add a customize-hook which runs update-initramfs multiple times with
# different content for /etc/flash-kernel/machine and creates a different
# initramfs per platform that way. But since we ultimately would like to have
# an identical /boot independent of the platform, we create a universal
# initramfs here. See also the comment about "single unified /boot partition"
# further below.
set -- "$@" \
	--essential-hook='mkdir -p "$1"/etc/initramfs-tools/hooks' \
	--essential-hook='copy-in reform-mkimage-hook /etc/initramfs-tools/hooks/' \
	--customize-hook='chown 0:0 "$1"/etc/initramfs-tools/hooks/reform-mkimage-hook'

# generate boot.tar for all platforms
# Since reform-tools 1.50:
#  - /usr/share/kernel/postinst.d/zz-reform-tools makes sure that the dtbs for
#    all platforms are present in /boot.
#  - reform-display-config supports u-boot-menu by writing out
#    /etc/u-boot-menu/conf.d/reform.conf
# Still, we cannot have a single unified /boot partition (and thus one image
# that works on all platforms) because:
#  - old imx8mq which never updated u-boot have no support for extlinux.conf
#  - not all platforms set $fdtfile in u-boot (f.e. first batch pocket reform)
#  - /boot/flash.bin would have to be moved elsewhere
# Even if the above were implemented, the images would still somehow require
# platform specific u-boot in the first few MB of the image.
set -- "$@" --customize-hook="
set -exu;
rootdir=\"\$1\"
kvers=\"\$(chroot \"\$rootdir\" linux-version list | chroot \"\$rootdir\" linux-version sort | tail -1)\";
for SYSIMAGE in $SYSIMAGES; do
	CONF=\"\$(find \"\$rootdir/usr/share/reform-tools/machines\" -name \"*.conf\" -not -name \"MNT Reform 2 HDMI.conf\" -type f -print0 | xargs --null grep --files-with-matches --line-regexp --fixed-strings \"SYSIMAGE=\\\"\$SYSIMAGE\\\"\")\"
	if [ ! -e \"\$CONF\" ]; then
		echo \"reform-tools does not know about a configuration for \$SYSIMAGE\" >&2
		exit 1
	fi
	. \"\$CONF\"
	if [ -z \${DTBPATH+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses DTBPATH\" >&2
		exit 1
	fi
	if [ -z \${UBOOT_OFFSET+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses UBOOT_OFFSET\" >&2
		exit 1
	fi
	if [ -z \${FLASHBIN_OFFSET+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses FLASHBIN_OFFSET\" >&2
		exit 1
	fi
	if [ -z \${UBOOT_PROJECT+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses UBOOT_PROJECT\" >&2
		exit 1
	fi
	if [ -z \${UBOOT_TAG+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses UBOOT_TAG\" >&2
		exit 1
	fi
	if [ -z \${UBOOT_SHA1+x} ]; then
		echo \"machine configuration for \$SYSIMAGE misses UBOOT_SHA1\" >&2
		exit 1
	fi
	MACHINE=\"\$(basename \"\$CONF\" .conf)\"
	/usr/lib/apt/apt-helper download-file \
		\"https://source.mnt.re/reform/\$UBOOT_PROJECT/-/jobs/artifacts/\$UBOOT_TAG/raw/\$(basename \"\$DTBPATH\" .dtb)-flash.bin?job=build\" \
		\"\$rootdir/boot/flash.bin\" \"SHA1:\$UBOOT_SHA1\"
	awk \"/^Machine: \$MACHINE\$/,/^\$/\" \"\$rootdir/usr/share/flash-kernel/db/all.db\" >&2
	awk \"/^Machine: \$MACHINE\$/,/^\$/\" \"\$rootdir/usr/share/flash-kernel/db/all.db\" | grep --silent \"^DTB-Id: \$DTBPATH\$\" >&2;
	echo \"\$MACHINE\" > \"\$rootdir/etc/flash-kernel/machine\";
	echo yes > \"\$rootdir/etc/flash-kernel/ignore-efi\";
	chown -R root:root \"\$rootdir/boot/flash.bin\";
	chroot \"\$rootdir\" flash-kernel;
	readlink \"\$rootdir/boot/dtb\" >&2;
	readlink \"\$rootdir/boot/dtb-\$kvers\" >&2;
	test \"\$(readlink \"\$rootdir/boot/dtb-\$kvers\")\" = \"dtbs/\$kvers/\$DTBPATH\"
	env --chdir \"\$rootdir\" find boot/ -wholename \"boot/dtbs/\$kvers/\$DTBPATH\" >&2;
	env --chdir \"\$rootdir\" find boot/ -wholename \"boot/dtbs/\$kvers/\$DTBPATH\" | grep --silent \"/\$DTBPATH\$\";
	test -e \"\$rootdir/\$(env --chdir \"\$rootdir\" find boot/ -wholename \"boot/dtbs/\$kvers/\$DTBPATH\")\";
	\"\$MMDEBSTRAP_ARGV0\" --hook-helper \"\$rootdir\" \"\$MMDEBSTRAP_MODE\" \"\$MMDEBSTRAP_HOOK\" env \"\$MMDEBSTRAP_VERBOSITY\" tar-out /boot \"./\$(basename \"\$DTBPATH\" .dtb)-boot.tar\" <&\"\$MMDEBSTRAP_HOOKSOCK\" >&\"\$MMDEBSTRAP_HOOKSOCK\"
	rm \"\$rootdir/etc/flash-kernel/machine\"
	rm \"\$rootdir/etc/flash-kernel/ignore-efi\"
	rm \"\$rootdir/boot/dtb\"
	rm \"\$rootdir/boot/dtb-\$kvers\"
	unset DTBPATH
	unset UBOOT_OFFSET
	unset FLASHBIN_OFFSET
	unset UBOOT_PROJECT
	unset UBOOT_TAG
	unset UBOOT_SHA1
done
"
# make sure that nothing inside the chroot is accidentally owned by the user
# running mmdebstrap (happens when using sync-in or copy-in hooks)
if [ "$(id -u)" -ne 0 ]; then
	set -- "$@" --customize-hook="find \"\$1\" -xdev \\( -gid $(id -g) -or -uid $(id -u) \\) -print"
	set -- "$@" --customize-hook="test -z \"\$(find \"\$1\" -xdev \\( -gid $(id -g) -or -uid $(id -u) \\) -print -quit )\""
fi
set -- "$@" --customize-hook='test -z "$(env --chdir "$1" find "." -xdev \! -path ./dev/full \! -path ./dev/null \! -path ./dev/random \! -path ./dev/tty \! -path ./dev/urandom \! -path ./dev/zero \( \! -type c \) \( \! -type l \) \( \! -type d -o \! -perm /o+t \) \( \( -gid 0 -perm /g+w \) -o -perm /o+w \) -print0 | env --chdir "$1" xargs --null --no-run-if-empty ls -ld | while read line; do echo "$line"; echo "$line" >&2; done)"'
# remove some files that make the output unreproducible
set -- "$@" --customize-hook='rm "$1/etc/nvme/hostid" "$1/etc/nvme/hostnqn"' \
	--customize-hook='find "$1/var/lib/dkms/reform2_lpc" -wholename "*/log/make.log" -delete'
# enable signing again
set -- "$@" --customize-hook='rm "$1/etc/dkms/framework.conf.d/nosigning.conf"'
# remove root password -- using `passwd -d root` produces unreproducible output
set -- "$@" \
	--customize-hook='echo "root:root" | chroot "$1" chpasswd' \
	--customize-hook='chroot "$1" sed -i "s/^root:[^:]\+:/root::/" /etc/shadow' \
	--customize-hook='rm "$1"/etc/resolv.conf'
# Remove initramfs hooks and scripts which are not supposed to be present in
# any initramfs generated on the target platform using update-initramfs.
set -- "$@" --customize-hook='rm "$1/etc/initramfs-tools/scripts/local-premount/reform-firstboot-premount" "$1/etc/initramfs-tools/hooks/reform-firstboot-hook" "$1/etc/initramfs-tools/hooks/reform-mkimage-hook"'

case "$DIST" in
	testing)
		set -- "$@" testing
		;;
	unstable|experimental)
		set -- "$@" unstable
		;;
	bookworm|bookworm-backports)
		set -- "$@" bookworm
		;;
esac

trap 'rm -f boot.ext4 root.ext4 etc.tar reform-firstboot-premount reform-firstboot-hook reform-firstboot.service reform-mkimage-hook' EXIT
# The default action for these signals does not invoke the EXIT trap.
trap 'exit 1' HUP INT QUIT TERM

reproducible_mke2fs() {
	uuid=$(uuidgen --sha1 --namespace @dns --name "$MIRROR")
	uuid=$(uuidgen --sha1 --namespace "$uuid" --name "$SOURCE_DATE_EPOCH")
	/sbin/mke2fs -q -F -o Linux -T ext4 -b 4096 -d - \
		-U "$uuid" -E "hash_seed=$uuid" \
		"$@"
}

mmdebstrap --verbose --format=tar --mode=unshare "$@" - \
	| mmtarfilter --path-exclude='/boot/*' \
	| reproducible_mke2fs -L reformsdroot root.ext4 "${ROOTSIZE}M"

# sanity check
/usr/sbin/debugfs root.ext4 -R "stat /usr/bin/env" \
	| sed 's/ \+/ /g' \
	| grep --quiet "Type: regular Mode: 0755"

for SYSIMAGE in $SYSIMAGES; do
	CONF="$(find ./machines -name "*.conf" -not -name "MNT Reform 2 HDMI.conf" -type f -print0 | xargs --null grep --files-with-matches --line-regexp --fixed-strings "SYSIMAGE=\"$SYSIMAGE\"")"
	if [ ! -e "$CONF" ]; then
		echo "reform-tools does not know about a configuration for $SYSIMAGE" >&2
		exit 1
	fi
	# shellcheck source=/dev/null
	. "$CONF"
	if [ -z ${DTBPATH+x} ]; then
		echo "machine configuration for $SYSIMAGE misses DTBPATH" >&2
		exit 1
	fi
	if [ -z ${UBOOT_OFFSET+x} ]; then
		echo "machine configuration for $SYSIMAGE misses UBOOT_OFFSET" >&2
		exit 1
	fi
	if [ -z ${FLASHBIN_OFFSET+x} ]; then
		echo "machine configuration for $SYSIMAGE misses FLASHBIN_OFFSET" >&2
		exit 1
	fi
	if [ -z ${UBOOT_SHA1+x} ]; then
		echo "machine configuration for $SYSIMAGE misses UBOOT_SHA1" >&2
		exit 1
	fi

	<"./$(basename "$DTBPATH" .dtb)-boot.tar" \
		mmtarfilter --strip-components=1 \
		| reproducible_mke2fs -L reformsdboot "boot.ext4" "${BOOTSIZE}M"

	# make sure that the filesystem includes boot.scr
	/usr/sbin/debugfs boot.ext4 -R "stat boot.scr" \
		| sed 's/ \+/ /g' | grep --quiet "Type: regular Mode: 0644 Flags: 0x"

	# Space in MB which needs to be kept empty for u-boot before the first
	# partition starts. Biggest requirement comes from rk3588 which expects
	# u-boot at an offset of 8 MiB and a 4 MiB trust (ATF, OP-TEE)
	# partition after that. So the first partition can only start at byte
	# 16777216 or 16 MiB: https://opensource.rock-chips.com/wiki_Partitions
	MAX_UBOOT=16

	: >"$SYSIMAGE.img"
	python3 sparsedd.py root.ext4 "$SYSIMAGE.img" 0 "$(((BOOTSIZE+MAX_UBOOT)*1024*1024))"
	python3 sparsedd.py boot.ext4 "$SYSIMAGE.img" 0 "$((MAX_UBOOT*1024*1024))"
	truncate --size="+512" "$SYSIMAGE.img"
	/sbin/parted --script --machine "$SYSIMAGE.img" "mklabel msdos"
	# reproducible disk signature
	printf mntr | dd of="$SYSIMAGE.img" seek=440 bs=1 conv=notrunc
	/sbin/parted --script --machine "$SYSIMAGE.img" "mkpart primary ext4 ${MAX_UBOOT}MiB $((BOOTSIZE+MAX_UBOOT))MiB"
	/sbin/parted --script --machine "$SYSIMAGE.img" "mkpart primary ext4 $((BOOTSIZE+MAX_UBOOT))MiB $((BOOTSIZE+ROOTSIZE+MAX_UBOOT))MiB"
	/sbin/parted --script --machine "$SYSIMAGE.img" print

	# install u-boot for platforms that can load it from sd-card
	if [ "$SD_BOOT" = true ]; then
		# sanity check to prevent accidentally overwriting the MBR
		if [ "$UBOOT_OFFSET" -lt 512 ]; then
			echo "E: refusing to write u-boot into the first 512 bytes" >&2
			exit 1
		fi
		# get correct u-boot binary from flash.bin in boot.tar
		extract_flash_bin() {
			tar --to-stdout --extract \
				--file "./$(basename "$DTBPATH" .dtb)-boot.tar" \
				boot/flash.bin
		}
		# sanity check
		test "$(extract_flash_bin | sha1sum)" = "$UBOOT_SHA1  -"
		# flash /boot/flash.bin to disk at the correct offset
		extract_flash_bin | dd \
		   of="$SYSIMAGE.img" \
		   conv=notrunc \
		   bs=512 \
		   seek="$((UBOOT_OFFSET/512))" \
		   skip="$((FLASHBIN_OFFSET/512))"
	fi

	case "$MIRROR" in
	reform.debian.net) EXT=".xz" >&2 ;;
	mntre.com)         EXT=".gz" >&2 ;;
	esac
	if [ "$QUICK" = "yes" ]; then
		EXT=
	fi
	# create bmap file
	bmaptool create --output="./$SYSIMAGE.img$EXT.bmap" "./$SYSIMAGE.img"

	# compress
	if [ "$QUICK" != "yes" ]; then
		case "$MIRROR" in
		reform.debian.net) xz --force --verbose -9 --extreme "./$SYSIMAGE.img" ;;
		mntre.com) pigz --no-name --force "./$SYSIMAGE.img" ;;
		esac
	fi

	unset DTBPATH
	unset UBOOT_OFFSET
	unset FLASHBIN_OFFSET
	unset UBOOT_SHA1
done

echo "Custom packages not from debian.org:" >&2
cat custom-pkgs.lst >&2
echo >&2

echo "MD5 checksums:" >&2
for SYSIMAGE in $SYSIMAGES; do
	case "$MIRROR" in
	reform.debian.net) EXT=".xz" >&2 ;;
	mntre.com)         EXT=".gz" >&2 ;;
	esac
	if [ "$QUICK" = "yes" ]; then
		EXT=
	fi
	md5sum "$SYSIMAGE.img$EXT" >&2
done
