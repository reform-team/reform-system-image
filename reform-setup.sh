# This script launches the Setup Wizard on first boot and auto-login as root.
#
if [ "$(whoami)" = "root" ] && [ "$(tty)" = '/dev/tty1' ]
then
        clear
        echo "Starting MNT Reform Setup Wizard..."

        # 1. Run setup wizard

        /usr/bin/sway --config /usr/share/reform-setup-wizard/reform-setup-sway-config

        # 2. Remove root autologin

        rm /etc/systemd/system/getty@tty1.service.d/override.conf
        systemctl daemon-reload

        # 3. Enable and start greetd

        systemctl enable greetd
        systemctl start greetd

        # 4. Change to VT 7
        chvt 7

        # 5. Remove this script
        rm /etc/profile.d/reform-setup.sh
        exit
fi
